import pytest
from app.models import black_scholes

@pytest.mark.parametrize("case", [
    {"S": 100, "K": 100, "T": 1, "r": 0.05, "sigma": 0.2, "option_type": "call", "expected": 10.45},
    {"S": 100, "K": 100, "T": 1, "r": 0.05, "sigma": 0.2, "option_type": "put", "expected": 5.57}
])
def test_option_price(case):
    result = black_scholes(case["S"], case["K"], case["T"], case["r"], case["sigma"], case["option_type"])
    assert abs(result - case["expected"]) < 0.1

from flask import Flask

def create_app():
    app = Flask(__name__)

    from .routes import bp as options_bp
    app.register_blueprint(options_bp, url_prefix='/options')

    return app

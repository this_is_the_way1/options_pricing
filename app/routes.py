from flask import Blueprint, request, jsonify
from .models import black_scholes

bp = Blueprint('options', __name__)

@bp.route('/price', methods=['POST'])
def get_option_price():
    data = request.get_json()
    S = data['S']
    K = data['K']
    T = data['T']
    r = data['r']
    sigma = data['sigma']
    option_type = data['option_type']

    price = black_scholes(S, K, T, r, sigma, option_type)
    return jsonify({'price': price})
